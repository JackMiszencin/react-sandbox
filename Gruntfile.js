'use strict';

var jitGrunt = require('jit-grunt');
var timeGrunt = require('time-grunt');


module.exports = function gruntfile(grunt) {
    jitGrunt(grunt, {
        dist: 'artifact'
    });

    timeGrunt(grunt);

    function requireConfig(file) {
        return require('./gruntConfig/' + file);
    }

    grunt.initConfig({
        packageJson: require('./package.json'),

        connect: requireConfig('connect')(),
        copy: requireConfig('copy')(),
        cssmin: requireConfig('cssmin')(),
        dist: requireConfig('artifact')(grunt),
        eslint: requireConfig('eslint')(grunt),
        injector: requireConfig('injector')(),
        jsbeautifier: requireConfig('jsbeautifier')(),
        jshint: requireConfig('jshint')(),
        replace: requireConfig('replace')(),
        run: requireConfig('run')(grunt),
        uglify: requireConfig('uglify')(),
        watch: requireConfig('watch')(),
        webpack: requireConfig('webpack')()
    });

    grunt.registerTask('default', [
        'checkCode',
        'build:local:debug',
        'connect:all',
        'watch'
    ]);

    grunt.registerTask('checkCode', [
        'jsbeautifier',
        'eslint'
    ]);

    grunt.registerTask('build:local:debug', [
        'checkCode',
        'cssmin',
        'injector:build',
        'webpack:dev'
    ]);

    grunt.registerTask('prod', [
        'checkCode',
        'cssmin',
        'injector:build',
        'webpack:prod'
    ]);

    grunt.registerTask('build', [
        'run:test',
        'prod',
        'dist:prepare',
        'copy:all'
    ]);

    grunt.registerTask('jenkins', [
        'build',
        'dist:publish'
    ]);
};
