import * as actions from './actions';

export function defaultAction(data) {
    console.log("Inside defaultAction");
    return {
        type: actions.DEFAULT_ACTION,
        data
    };
}

export function otherAction(data) {
    console.log("Inside otherAction");
    return {
        type: actions.DEFAULT_ACTION,
        data
    };
}
