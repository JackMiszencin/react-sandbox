import _ from 'lodash';
import testHelpers from '../test-helpers';
import * as actions from './actions';

const expect = testHelpers.expect;

describe('main/actions', () => {
    it('has known properties', () => {
        const clone = _.clone(actions);

        expect(clone).to.have.property('DEFAULT_ACTION').to.be.a('string');
        delete clone.DEFAULT_ACTION;

        expect(clone, "No more attributes").to.deep.equal({});
    });
});
