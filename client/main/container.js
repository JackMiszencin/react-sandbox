import { connect } from 'react-redux';
import mainComponent from './component';
import * as actionCreators from './action-creators';
import namespace from './namespace';
import { Map } from 'immutable';


function mapStateToProps(state) {
    const normalizedState = state.get(namespace, Map()).toJS();

    return {
        items: normalizedState.items
    };
}

function mapDispatchToProps(dispatch) {
    return {
        defaultAction: actionCreators.defaultAction.bind(null, dispatch)
    };
}

export default function(componentMain = mainComponent) {
    return connect(mapStateToProps, mapDispatchToProps)(componentMain);
}
