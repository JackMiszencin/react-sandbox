import React from 'react';

export default class ButtonComponent extends React.Component {
    constructor(props) {
        super(props);
        console.log("props.toDisplay", props.toDisplay);
        this.toDisplay = props.toDisplay;
    }

    incrementToDisplay() {
        console.log("hitting incrementToDisplay");
        this.toDisplay.thing += 1;
        console.log("this.toDisplay.thing ", this.toDisplay.thing);
    }

    updatedToDisplay() {
        return (
            <div>
                {this.toDisplay.thing}
            </div>
        );
    }

    render() {
        console.log("in render");
        return (
            <div>
                <button onClick={() => this.incrementToDisplay()}>make things happen!</button>
                <div>blah blah{this.props.toDisplay.thing}</div>
            </div>
        );
    }
}

ButtonComponent.displayName = 'ButtonComponent';

ButtonComponent.propTypes = {
    toDisplay: React.PropTypes.object
};
