import React from 'react';
import ReactDOM from 'react-dom';
import {
    applyMiddleware,
    createStore,
    compose
} from 'redux';
import { Provider } from 'react-redux';
import createLogger from 'redux-logger';
import { combineReducers } from 'redux-immutable';
import { Map } from 'immutable';
import DevTools from './DevTools';
import main from './main';
import _ from 'lodash';

const middlewares = [];

if (process.env.NODE_ENV !== 'production') {
    const logger = createLogger({
        duration: true,
        timestamp: true
    });

    middlewares.push(logger);
}

const composed = [applyMiddleware(...middlewares)];

if (process.env.NODE_ENV !== 'production') {
    composed.push(DevTools.instrument());
}

const mainReducersClone = _.clone(main.combinedReducers);
const reducers = combineReducers(mainReducersClone);

const enhancer = compose(...composed);
const store = createStore(reducers, Map(), enhancer);

store.dispatch(main.actionCreators.defaultAction({
    fizz: "buzz"
}));

function getDevTools() {
    if (process.env.NODE_ENV !== 'production') {
        return <DevTools />;
    }
    return null;
}

ReactDOM.render(
    <Provider store={store}>
        <div>
            <main.Container />
            {getDevTools()}
        </div>
    </Provider>,
    document.getElementById('app') // eslint-disable-line no-undef
);
