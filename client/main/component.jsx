import React from 'react';
import button from '../button';

export default class MainComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div>
                    <div>funk</div>
                </div>
                <button.component toDisplay={{ thing: 1 }} />
            </div>
        );
    }
}
MainComponent.displayName = 'MainComponent';

MainComponent.propTypes = {
    defaultAction: React.PropTypes.func
};
