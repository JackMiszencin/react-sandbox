// import * as actions from './actions';
// import * as status from './status';
import { fromJS, Map } from 'immutable';


export default function reducers(state = Map(), action) {
    if (action && action.type) {
        switch (action.type) {
            case 'DEFAULT_ACTION':
                return fromJS(action.data);
            default:
                return state;
        }
    }
    return state;
}
